/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#pragma once

#include <string>
#include <span>

class EncodeParams final {
public:
    [[nodiscard]] std::string getAlgorithm() const;
    [[nodiscard]] bool isAlgorithmPresent() const;
    void setAlgorithm(std::string value);

    [[nodiscard]] std::string getFilename() const;
    [[nodiscard]]bool isFilenamePresent() const;
    void setFilename(std::string value);

    static EncodeParams parse(std::span<const std::string>);

private:
    std::string algorithm;
    std::string filename;
};
