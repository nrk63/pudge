/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#pragma once

#include <span>
#include <cstdint>
#include <string>

namespace pudgeClient {
    constexpr char kMajorVersion { MAJOR_VERSION };
    constexpr char kMinorVersion { MINOR_VERSION };
    constexpr char kFixVersion { FIX_VERSION };
    constexpr char kVersion { ((kMajorVersion >> 4) & 0x0f) | (kMinorVersion & 0x0f) };

    constexpr char kFileExtension[] = ".pudg";
    constexpr char kFileSignature[] = { 0x50, 0x55, 0x44, 0x47, 0x45 };

    void encode(std::span<const std::string> args);
    void decode(std::span<const std::string> args);

    void help();
    void version();
} // namespace pudgeClient
