/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <iostream>
#include <string>
#include <vector>

#include <pudgeClient/main.hpp>

int main(int argc, char** argv) {
    using namespace pudgeClient;

    if (argc < 2) {
        help();
        return 0;
    }

    std::vector<std::string> args { argv + 1, argv + argc };

    if (args[0] == "help" || args[0] == "-h" || args[0] == "--help") {
        help();
        return 0;
    }

    if (args[0] == "version" || args[0] == "-v" || args[0] == "--version") {
        version();
        return 0;
    }

    if (args[0] == "encode") {
        try {
            encode(std::span<const std::string> { args.cbegin() + 1, args.cend() });
        } catch (const std::exception& e) {
            std::cerr << "Fatal: " << e.what() << "." << std::endl;
            return 1;
        } catch (...) {
            std::cerr << "Fatal: unexpected error." << std::endl;
            return 1;
        }
        return 0;
    }

    if (args[0] == "decode") {
        try {
            decode(std::span<const std::string> { args.cbegin() + 1, args.cend() });
        } catch (const std::exception& e) {
            std::cerr << "Fatal: " << e.what() << "." << std::endl;
            return 1;
        } catch (...) {
            std::cerr << "Fatal: unexpected error." << std::endl;
            return 1;
        }
        return 0;
    }

    std::cerr << "Unknown command: " << argv[1] << "." << std::endl;
    return 0;
}
