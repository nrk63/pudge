/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <pudgeClient/main.hpp>

#include <iostream>
#include <fstream>
#include <string>

#include <pudge/none.hpp>
#include <pudge/pudge1.hpp>
#include <pudge/shannon.hpp>

#include <pudgeClient/EncodeParams.hpp>

using namespace pudge;

namespace {
    std::unique_ptr<Encoder> createFromName(std::string_view name) {
        if (name == "none")
            return std::make_unique<NoneEncoder>();

        if (name == "pudge1")
            return std::make_unique<Pudge1Encoder>();

        if (name == "shannon")
            return std::make_unique<ShannonEncoder>();

        throw std::invalid_argument { "unknown algorithm: \"" + std::string { name } + "\"" };
    }
} // namespace

void pudgeClient::encode(std::span<const std::string> args) {
    EncodeParams params = EncodeParams::parse(args);

    if (!params.isAlgorithmPresent()) {
        std::cout << "Warning: No algorithm specified. "
                  << "File data will not be encoded."
                  << std::endl;
        params.setAlgorithm("none");
    }

    std::string algorithm = params.getAlgorithm();
    std::string filename = params.getFilename();

    std::ifstream in { filename, std::ios_base::binary };
    if (!in.good())
        throw std::runtime_error { "unable to open input file" };

    using namespace pudgeClient;

    std::ofstream out { filename + kFileExtension, std::ios_base::binary };
    if (!out.good())
        throw std::runtime_error { "unable to open output file" };

    std::cout << "Starting encoding with params:\n"
              << "    Algorithm: " << algorithm << "\n"
              << "    Filename: " << filename << std::endl;

    out << kFileSignature << kVersion;
    auto filenameSize { static_cast<uint32_t>(filename.size()) };
    out.write(reinterpret_cast<char*>(&filenameSize), sizeof(filenameSize));
    out << filename;

    auto algorithmNameSize { static_cast<uint32_t>(algorithm.size()) };
    out.write(reinterpret_cast<char*>(&algorithmNameSize), sizeof(algorithmNameSize));
    out << algorithm;

    std::unique_ptr<Encoder> encoder = createFromName(algorithm);
    encoder->encode(in, out);

    std::cout << "Successful." << std::endl;
}
