/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <pudgeClient/main.hpp>

#include <memory>
#include <fstream>

#include <pudge/none.hpp>
#include <pudge/pudge1.hpp>
#include <pudge/shannon.hpp>

#include <pudgeClient/DecodeParams.hpp>

using namespace pudge;

namespace {
    std::unique_ptr<Encoder> createDecoderFromName(std::string_view name) {
        if (name == "none")
            return std::make_unique<NoneEncoder>();

        if (name == "pudge1")
            return std::make_unique<Pudge1Encoder>();

        if (name == "shannon")
            return std::make_unique<ShannonEncoder>();

        throw std::domain_error { "unknown algorithm: \"" + std::string { name } + "\"" };
    }

    void checkFileSignature(std::istream& in) {
        char byte;
        for (char c : pudgeClient::kFileSignature) {
            if (!in.get(byte))
                throw std::invalid_argument { "unexpected eof in file signature section" };
            if (byte != c)
                throw std::invalid_argument { "wrong file signature" };
        }
    }

    void checkProgramVersion(std::istream& in) {
        char byte;
        if (!in.get(byte))
            throw std::logic_error { "unexpected eof in version section" };

        using namespace pudgeClient;
        if (byte != kVersion) {
            uint8_t actualMajor = (byte >> 4) & 0x0f;
            uint8_t actualMinor = byte & 0x0f;
            std::cout << "Warning: current version of the program differs:"
                      << kMajorVersion << "." << kMinorVersion << "(current) vs "
                      << actualMajor << "." << actualMinor << "(actual)." << std::endl;
        }
    }

    std::string readOriginalFilename(std::istream& in) {
        uint32_t originalFilenameSize;
        if (!in.read(reinterpret_cast<char*>(&originalFilenameSize),
                     sizeof(originalFilenameSize))) {
            throw std::logic_error { "unexpected eof in original filename section" };
        }

        std::string originalFilename(originalFilenameSize, char { 0 });
        in.read(originalFilename.data(), originalFilenameSize);
        if (in.gcount() != originalFilenameSize)
            throw std::logic_error { "unexpected eof in original filename section" };

        return originalFilename;
    }

    std::string getAlgorithm(std::istream& in) {
        uint32_t algorithmNameSize;
        if (!in.read(reinterpret_cast<char*>(&algorithmNameSize),
                     sizeof(algorithmNameSize))) {
            throw std::logic_error { "unexpected eof in algorithm section" };
        }

        std::string algorithmName(algorithmNameSize, char { 0 });
        in.read(algorithmName.data(), algorithmNameSize);
        if (in.gcount() != algorithmNameSize)
            throw std::logic_error { "unexpected eof in algorithm section" };

        return algorithmName;
    }
} // namespace

void pudgeClient::decode(std::span<const std::string> args) {
    DecodeParams params = DecodeParams::parse(args);

    std::string filename = params.getFilename();
    std::ifstream in { filename, std::ios_base::binary };
    if (!in.good())
        throw std::runtime_error { "unable to open input file" };

    checkFileSignature(in);
    checkProgramVersion(in);

    std::string originalFilename = readOriginalFilename(in);
    std::string algorithm = getAlgorithm(in);

    std::ofstream out { originalFilename, std::ios_base::binary };
    if (!out.good())
        throw std::logic_error { "unable to open output file" };

    std::cout << "Starting decoding with params:\n"
              << "    Algorithm: " << algorithm << "\n"
              << "    Filename: " << filename << "\n"
              << "    Original filename: " << originalFilename << std::endl;

    std::unique_ptr<Encoder> decoder = createDecoderFromName(algorithm);
    decoder->decode(in, out);

    std::cout << "Successful." << std::endl;
}
