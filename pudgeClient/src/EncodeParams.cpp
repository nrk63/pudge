/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <pudgeClient/EncodeParams.hpp>

#include <string>
#include <span>
#include <stdexcept>

std::string EncodeParams::getAlgorithm() const {
    return algorithm;
}

bool EncodeParams::isAlgorithmPresent() const {
    return !algorithm.empty();
}

void EncodeParams::setAlgorithm(std::string value) {
    if (!algorithm.empty())
        throw std::logic_error { "multiple values of algorithm" };
    algorithm = std::move(value);
}

std::string EncodeParams::getFilename() const {
    return filename;
}

bool EncodeParams::isFilenamePresent() const {
    return !filename.empty();
}

void EncodeParams::setFilename(std::string value) {
    if (!filename.empty())
        throw std::logic_error { "multiple filenames" };
    filename = std::move(value);
}

EncodeParams EncodeParams::parse(std::span<const std::string> argv) {
    if (argv.empty())
        throw std::invalid_argument { "no filename" };

    EncodeParams params;
    for (auto it = argv.begin(); it != argv.end(); ++it) {
        if (*it == "-a") {
            if (it + 1 == argv.end())
                throw std::invalid_argument { "no value for -a" };
            params.setAlgorithm(*(++it));
        } else if (it->starts_with("-")) {
            throw std::invalid_argument { "unknown option: " + *it };
        } else {
            params.setFilename(*it);
        }
    }

    if (!params.isFilenamePresent())
        throw std::invalid_argument { "no filename" };

    return params;
}
