/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <pudgeClient/main.hpp>

#include <iostream>
#include <string_view>

namespace {
    using namespace std::string_view_literals;
    constexpr std::string_view kHelpMessage =
            "Command list:\n"
            "    encode [-a <algorithm> <param1> ...] <filename>\n"
            "    decode <filename> <filename>\n"
            "    help (-h | --help)\n"
            "    version (-v | --version)"sv;
} // namespace

void pudgeClient::help() {
    std::cout << kHelpMessage << std::endl;
}
