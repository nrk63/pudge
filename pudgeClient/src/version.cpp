/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <pudgeClient/main.hpp>

#include <iostream>

void pudgeClient::version() {
    using namespace pudgeClient;
    std::cout << static_cast<size_t>(kMajorVersion) << "."
              << static_cast<size_t>(kMinorVersion) << "."
              << static_cast<size_t>(kFixVersion) << std::endl;
}
