/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <gtest/gtest.h>

#include <random>
#include <memory>
#include <boost/dynamic_bitset.hpp>
#include <pudge/InputBitStream.hpp>
#include <pudge/OutputBitStream.hpp>

using namespace pudge;

class BitStreamTest : public ::testing::Test {
public:
    static void SetUpTestSuite() {
        auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
        engine = std::make_unique<std::mt19937>(static_cast<std::mt19937::result_type>(seed));
    }

    template<std::forward_iterator ForwardIt>
    requires std::is_convertible_v<char, typename std::iterator_traits<ForwardIt>::value_type>
    static void generateRandomBytes(ForwardIt begin, ForwardIt end) {
        std::uniform_int_distribution<char> dist { std::numeric_limits<char>::min(),
                                                   std::numeric_limits<char>::max() };
        for (; begin != end; ++begin)
            *begin = dist(*engine);
    }

    static size_t getRandomSizeBytes() {
        std::uniform_int_distribution<size_t> dist { kMinBytes, kMaxBytes };
        return dist(*engine);
    }

private:
    static constexpr size_t kMinBytes = 2048;
    static constexpr size_t kMaxBytes = 4096;

    static std::unique_ptr<std::mt19937> engine;
};

std::unique_ptr<std::mt19937> BitStreamTest::engine {};

TEST_F(BitStreamTest, Positive_TestAllBits) {
    size_t testDataSizeBytes = getRandomSizeBytes();

    std::vector<uint8_t> testData(testDataSizeBytes);
    generateRandomBytes(testData.begin(), testData.end());

    std::stringstream in {};
    in.write(reinterpret_cast<const char*>(testData.data()), testData.size());
    ASSERT_TRUE(in.good());

    boost::dynamic_bitset<uint8_t> bitset;
    bitset.init_from_block_range(testData.cbegin(), testData.cend());

    InputBitStream bitStream { in };
    for (size_t i = 0; i < testDataSizeBytes*CHAR_BIT; ++i)
        ASSERT_EQ(bitStream.test(), bitset.test(i)) << "at index " << i;
}

TEST_F(BitStreamTest, Negative_BadStream) {
    std::stringstream in {};
    in.setstate(std::ios_base::badbit);
    ASSERT_THROW(InputBitStream { in }, std::invalid_argument);
}

TEST_F(BitStreamTest, Negative_ReadingFailure) {
    const size_t testDataSizeBytes = 5;
    std::stringstream in {};
    for (size_t i = 0; i < testDataSizeBytes; ++i)
        in.put(0b01110001);

    InputBitStream bitStream { in, testDataSizeBytes - 1 };
    bool bit;
    for (size_t i = 0; i < (testDataSizeBytes - 1)*CHAR_BIT; ++i)
        // Assign it to suppress the unused return value warning
        bit = bitStream.test();

    in.setstate(std::ios_base::badbit);
    ASSERT_THROW(bit = bitStream.test(), std::runtime_error);
}

TEST_F(BitStreamTest, Positive_WriteExtraBits) {
    size_t testDataSizeBytes = getRandomSizeBytes();

    std::vector<char> testData(testDataSizeBytes);
    generateRandomBytes(testData.begin(), testData.end());

    boost::dynamic_bitset<uint8_t> bitset;
    bitset.init_from_block_range(testData.cbegin(), testData.cend());
    // Add extra bits
    for (size_t i = 0; i < getRandomSizeBytes()%CHAR_BIT; ++i)
        bitset.push_back(true);

    std::stringstream out {};
    {
        OutputBitStream bitStream { out };
        for (size_t i = 0; i < bitset.size(); ++i)
            bitStream.write(bitset.test(i));
    }

    std::stringstream in { std::move(out) };
    uint8_t byte;
    for (size_t i = 0; i < bitset.size(); ++i) {
        auto bitIndex = i % CHAR_BIT;
        if (bitIndex == 0)
            in.get(reinterpret_cast<char&>(byte));
        ASSERT_EQ(bool((byte >> bitIndex) & 1),
                  bitset.test(i)) << "at index " << i;
    }
}
