/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <sstream>
#include <random>
#include <chrono>
#include <memory>
#include <concepts>
#include <limits>

#include <gtest/gtest.h>

#include <pudge/pudge1.hpp>
#include <pudge/shannon.hpp>

using namespace pudge;

class EncDecTest : public ::testing::TestWithParam<std::shared_ptr<Encoder>> {
public:
    static void SetUpTestSuite() {
        auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
        engine = std::make_unique<std::mt19937>(static_cast<std::mt19937::result_type>(seed));
    }

    void SetUp() override {
        encoder = GetParam();
        in = std::stringstream {};
        out = std::stringstream {};
    }

    template<std::forward_iterator ForwardIt>
    requires std::is_convertible_v<char, typename std::iterator_traits<ForwardIt>::value_type>
    static void generateRandomBytes(ForwardIt begin, ForwardIt end) {
        std::uniform_int_distribution<char> dist { std::numeric_limits<char>::min(),
                                                   std::numeric_limits<char>::max() };
        for (; begin != end; ++begin)
            *begin = dist(*engine);
    }

    static size_t getRandomSize() {
        std::uniform_int_distribution<size_t> dist { kMinSize, kMaxSize };
        return dist(*engine);
    }

protected:
    std::shared_ptr<Encoder> encoder;
    std::stringstream in;
    std::stringstream out;

private:
    static constexpr size_t kMinSize = 1024;
    static constexpr size_t kMaxSize = 4096;

    static std::unique_ptr<std::mt19937> engine;
};

std::unique_ptr<std::mt19937> EncDecTest::engine;

TEST_P(EncDecTest, Positive_NormalLaunch) {
    size_t testDataSize = getRandomSize();
    std::vector<char> testData(testDataSize);
    generateRandomBytes(testData.begin(), testData.end());

    in.write(testData.data(), testData.size());
    ASSERT_TRUE(in.good());
    ASSERT_NO_THROW(encoder->encode(in, out));

    in = std::move(out);
    out = std::stringstream {};
    ASSERT_NO_THROW(encoder->decode(in, out));

    std::vector<char> resultData(testDataSize);
    ASSERT_TRUE(out.read(resultData.data(), resultData.size()))
        << "Output data size doesn't match with input data size. "
        << "Successfully extracted: " << out.gcount() << "."
        << "Expected: " << testData.size();
    for (size_t i = 0; i < testData.size(); ++i)
        ASSERT_EQ(resultData[i], testData[i]) << "at index " << i << " of " << testData.size();
}

TEST_P(EncDecTest, Negative_BadInputStream) {
    in = std::stringstream { "Not an empty string" };
    in.setstate(std::ios_base::badbit);
    ASSERT_THROW(encoder->encode(in, out), std::invalid_argument);
}

TEST_P(EncDecTest, Negative_BadOutputStream) {
    in = std::stringstream { "Not an empty string" };
    out.setstate(std::ios_base::badbit);
    ASSERT_THROW(encoder->encode(in, out), std::invalid_argument);
}

INSTANTIATE_TEST_SUITE_P(
    , EncDecTest,
    testing::Values(
        std::make_shared<Pudge1Encoder>(),
        std::make_shared<ShannonEncoder>()));
