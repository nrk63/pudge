#pragma once

#include <pudge/Encoder.hpp>

#include <random>

namespace pudge {

class Pudge1Encoder final : public Encoder {
public:
    static constexpr uint8_t kMinBlockSize = 1;
    static constexpr uint8_t kMaxBlockSize = 16;

public:
    Pudge1Encoder();

    void encode(std::istream&, std::ostream&) override;
    void decode(std::istream&, std::ostream&) override;

private:
    std::mt19937 randomEngine;
};

} // pudge
