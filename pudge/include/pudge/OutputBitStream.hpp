/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#pragma once

#include <ostream>
#include <boost/dynamic_bitset.hpp>

namespace pudge {

class OutputBitStream final {
public:
    explicit OutputBitStream(std::ostream& out, size_t bufferSizeBytes=kDefaultBufferSizeBytes);
    ~OutputBitStream();

    explicit operator bool() const;
    [[nodiscard]] bool fail() const;
    [[nodiscard]] bool bad() const;
    [[nodiscard]] bool good() const;

    OutputBitStream& write(bool bit);
    void flush();

private:
    [[nodiscard]] bool atBufferEnd() const;
    void writeData();

    std::ostream& outputStream;
    size_t bufferSizeBytes;
    boost::dynamic_bitset<uint8_t> bits;

    static constexpr size_t kDefaultBufferSizeBytes = 1024;
};

} // pudge
