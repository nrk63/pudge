/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#pragma once

#include <cstdint>

namespace pudge {

class ShannonCode {
public:
    ShannonCode() = default;
    ShannonCode(uint8_t origin, uint8_t lengthBits, uint32_t value);
    ShannonCode(uint8_t origin, double p, double cumP);

    [[nodiscard]] uint8_t getOrigin() const;
    [[nodiscard]] uint8_t getLengthBits() const;
    [[nodiscard]] uint32_t getValue() const;

private:
    uint8_t origin;
    uint8_t lengthBits;
    uint32_t value;
};

} // pudge
