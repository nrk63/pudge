#pragma once

#include <pudge/Encoder.hpp>

namespace pudge {

class NoneEncoder : public Encoder {
    void encode(std::istream&, std::ostream&) override;
    void decode(std::istream&, std::ostream&) override;
};

} // pudge
