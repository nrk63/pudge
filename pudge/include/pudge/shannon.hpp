#pragma once

#include <pudge/Encoder.hpp>

namespace pudge {

class ShannonEncoder final : public Encoder {
public:
    void encode(std::istream&, std::ostream&) override;
    void decode(std::istream&, std::ostream&) override;
};

} // pudge
