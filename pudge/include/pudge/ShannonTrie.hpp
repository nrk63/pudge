/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#pragma once

#include <memory>
#include <boost/dynamic_bitset.hpp>
#include <pudge/ShannonCode.hpp>
#include <pudge/InputBitStream.hpp>

namespace pudge {

class ShannonTrie {
public:
    void addCode(const ShannonCode& shCode_, size_t start=0);

    ShannonCode searchCode(InputBitStream& in) const;

private:
    std::unique_ptr<ShannonCode> shCode;
    std::unique_ptr<ShannonTrie> zeroTrie;
    std::unique_ptr<ShannonTrie> oneTrie;
};

} // pudge
