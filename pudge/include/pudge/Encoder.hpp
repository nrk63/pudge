/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#pragma once

#include <cstdint>
#include <iostream>
#include <memory>
#include <random>

namespace pudge {

class Encoder {
public:
    virtual ~Encoder() = default;

    virtual void encode(std::istream&, std::ostream&) = 0;
    virtual void decode(std::istream&, std::ostream&) = 0;
};

} // pudge
