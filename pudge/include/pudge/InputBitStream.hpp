/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#pragma once

#include <istream>
#include <boost/dynamic_bitset.hpp>

namespace pudge {

class InputBitStream final {
public:
    explicit InputBitStream(std::istream& in, size_t bufferSizeBytes=kDefaultBufferSizeBytes);

    explicit operator bool() const;
    [[nodiscard]] bool eof() const;
    [[nodiscard]] bool fail() const;
    [[nodiscard]] bool bad() const;
    [[nodiscard]] bool good() const;

    [[nodiscard]] bool test();

private:
    [[nodiscard]] bool atBufferEnd() const;
    void readData();

    std::istream& inputStream;
    size_t pos = 0;
    boost::dynamic_bitset<uint8_t> bits;

    static constexpr size_t kDefaultBufferSizeBytes = 1024;
};

} // pudge
