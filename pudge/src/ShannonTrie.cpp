/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <pudge/ShannonTrie.hpp>

namespace pudge {

void ShannonTrie::addCode(const ShannonCode& shCode_, size_t start/*=0*/) {
    uint8_t lengthBits = shCode_.getLengthBits();
    if (start == lengthBits) {
        if (shCode)
            throw std::invalid_argument { "given code already exists" };
        shCode = std::make_unique<ShannonCode>(shCode_);
        return;
    }

    if ((shCode_.getValue() >> (lengthBits - 1 - start)) & 1) {
        if (!oneTrie)
            oneTrie = std::make_unique<ShannonTrie>();
        oneTrie->addCode(shCode_, start + 1);
    } else {
        if (!zeroTrie)
            zeroTrie = std::make_unique<ShannonTrie>();
        zeroTrie->addCode(shCode_, start + 1);
    }
}

ShannonCode ShannonTrie::searchCode(InputBitStream& in) const {
    if (!in)
        throw std::runtime_error { "reached the end of data" };

    if (shCode)
        return *shCode;

    if (in.test() && oneTrie)
        return oneTrie->searchCode(in);
    if (zeroTrie)
        return zeroTrie->searchCode(in);
    throw std::runtime_error { "reached the end of search trie" };
}

} // pudge
