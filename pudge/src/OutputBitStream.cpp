/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <pudge/OutputBitStream.hpp>

#include <vector>
#include <stdexcept>

namespace pudge {

OutputBitStream::OutputBitStream(std::ostream& out, size_t bufferSizeBytes)
    :outputStream(out), bufferSizeBytes(bufferSizeBytes) {
    if (!outputStream)
        throw std::invalid_argument { "stream is in a bad state" };
    bits.reserve(bufferSizeBytes*CHAR_BIT);
}

OutputBitStream::~OutputBitStream() {
    try {
        writeData();
    } catch (...) {
        // Well, we tried...
    }
}

OutputBitStream::operator bool() const {
    return good();
}

bool OutputBitStream::fail() const {
    return outputStream.fail();
}

bool OutputBitStream::bad() const {
    return outputStream.bad();
}

bool OutputBitStream::good() const {
    return outputStream.good();
}

OutputBitStream& OutputBitStream::write(bool bit) {
    if (atBufferEnd())
        writeData();

    bits.push_back(bit);
    return *this;
}

void OutputBitStream::flush() {
    writeData();
}

bool OutputBitStream::atBufferEnd() const {
    return bits.size()/CHAR_BIT >= bufferSizeBytes;
}

void OutputBitStream::writeData() {
    if (!outputStream)
        throw std::runtime_error { "stream is in a bad state" };

    size_t bufferSize = bits.size()/CHAR_BIT + (bits.size()%CHAR_BIT > 0 ? 1 : 0);
    std::vector<char> buffer(bufferSize);
    boost::to_block_range(bits, buffer.begin());
    outputStream.write(buffer.data(), buffer.size());
    bits.clear();
}

} // pudge
