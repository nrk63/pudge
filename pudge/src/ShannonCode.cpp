/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <pudge/ShannonCode.hpp>

#include <stdexcept>
#include <cmath>
#include <cassert>

namespace pudge {

ShannonCode::ShannonCode(uint8_t origin, uint8_t lengthBits, uint32_t value)
    :origin(origin), lengthBits(lengthBits), value(value) {}

ShannonCode::ShannonCode(uint8_t origin, double p, double cumP)
        : origin(origin) {
    if (p <= 0)
        throw std::invalid_argument { "probability of the byte must be greater than 0" };

    if (cumP >= 1)
        throw std::invalid_argument { "cumulative probability must be less than 1" };

    lengthBits = static_cast<uint8_t>(std::ceil(-std::log2(p)));
    assert(lengthBits <= sizeof(value)*CHAR_BIT);

    // Taking the first codeLength digits of binary fraction of
    // cumulative probability
    value = 0;
    bool integral;
    for (size_t i = 0; i < lengthBits; ++i) {
        cumP *= 2;
        integral = cumP > 1;
        if (integral)
            cumP -= 1;
        value <<= 1;
        value = value | (integral ? 1 : 0);
    }
}

uint8_t ShannonCode::getOrigin() const { return origin; }

uint8_t ShannonCode::getLengthBits() const { return lengthBits; };

uint32_t ShannonCode::getValue() const { return value; };

} // pudge
