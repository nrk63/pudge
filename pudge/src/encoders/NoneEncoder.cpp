/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <pudge/none.hpp>

#include <stdexcept>

namespace pudge {

void NoneEncoder::encode(std::istream& in, std::ostream& out) {
    if (!in.good())
        throw std::invalid_argument { "input stream is in a bad state" };

    if (!out.good())
        throw std::invalid_argument { "output stream is in a bad state" };

    char byte;
    while (in.get(byte) && out)
        out.put(byte);

    if (!in.eof() && !in.good())
        throw std::runtime_error { "input stream failure has occurred" };

    if (!out.eof() && !out.good())
        throw std::runtime_error { "output stream failure has occurred" };
}

void NoneEncoder::decode(std::istream& in, std::ostream& out) {
    if (!in.good())
        throw std::invalid_argument { "input stream is in a bad state" };

    if (!out.good())
        throw std::invalid_argument { "output stream is in a bad state" };

    NoneEncoder encoderDecoder {};
    encoderDecoder.encode(in, out);
}

} // pudge
