/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <pudge/pudge1.hpp>

#include <random>
#include <chrono>
#include <stdexcept>

namespace pudge {

Pudge1Encoder::Pudge1Encoder() {
    using clock = std::chrono::high_resolution_clock;
    std::random_device seeder;
    const auto seed { seeder.entropy() > 0 ? seeder() : clock::now().time_since_epoch().count() };
    randomEngine = std::mt19937 { static_cast<std::mt19937::result_type>(seed) };
}

void Pudge1Encoder::encode(std::istream& in, std::ostream& out) {
    static_assert(kMinBlockSize < kMaxBlockSize);

    if (!in.good())
        throw std::invalid_argument { "input stream is in a bad state" };

    if (!out.good())
        throw std::invalid_argument { "output stream is in a bad state" };

    std::uniform_int_distribution<uint8_t> dist { kMinBlockSize, kMaxBlockSize };

    while (in && out) {
        uint8_t blockSize = dist(randomEngine);
        std::vector<char> block(blockSize);

        in.read(block.data(), blockSize);
        uint8_t bytesRead = in.gcount();
        if (bytesRead == 0)
            break;

        if (bytesRead < blockSize)
            blockSize = bytesRead;

        out.put(static_cast<char>(blockSize));
        out.write(block.data(), blockSize);
    }

    if (!in.eof() && !in.good())
        throw std::runtime_error { "input stream failure has occurred" };

    if (!out.eof() && !out.good())
        throw std::runtime_error { "output stream failure has occurred" };
}

void Pudge1Encoder::decode(std::istream& in, std::ostream& out) {
    if (!in.good())
        throw std::invalid_argument { "input stream is in a bad state" };

    if (!out.good())
        throw std::invalid_argument { "output stream is in a bad state" };

    char byte;
    while (in.get(byte) && out) {
        auto blockSize = static_cast<uint8_t>(byte);
        if (blockSize < Pudge1Encoder::kMinBlockSize || blockSize > Pudge1Encoder::kMaxBlockSize)
            throw std::runtime_error { "invalid block size: " + std::to_string(blockSize) };

        std::vector<char> block(blockSize);
        in.read(block.data(), blockSize);

        uint8_t bytesRead = in.gcount();
        if (bytesRead != blockSize)
            throw std::runtime_error { "unexpected eof" };

        out.write(block.data(), blockSize);
    }

    if (!in.eof() && !in.good())
        throw std::runtime_error { "input stream failure has occurred" };

    if (!out.eof() && !out.good())
        throw std::runtime_error { "output stream failure has occurred" };
}

} // pudge
