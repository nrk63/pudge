/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <pudge/shannon.hpp>

#include <queue>
#include <unordered_map>

#include <boost/dynamic_bitset.hpp>
#include <pudge/ShannonCode.hpp>
#include <pudge/ShannonTrie.hpp>
#include <pudge/OutputBitStream.hpp>

namespace pudge {

void ShannonEncoder::encode(std::istream& in, std::ostream& out) {
    if (!in.good())
        throw std::invalid_argument { "input stream is in a bad state" };

    if (!out.good())
        throw std::invalid_argument { "output stream is in a bad state" };

    auto startPos = in.tellg();
    std::array<uint32_t, 256> freqs {};
    uint32_t bytesCount = 0;

    char byte;
    while (in.get(byte)) {
        ++freqs[static_cast<uint8_t>(byte)];
        ++bytesCount;
    }

    if (!in.eof() && !in.good())
        throw std::logic_error { "input stream is in a bad state" };
    // Clear eofbit (and failbit as well)
    in.clear();

    // Sort bytes by their probabilities in decreasing order with priority queue
    auto comparator = [](auto p1, auto p2) {
        return p1.second < p2.second;
    };
    std::priority_queue<std::pair<uint8_t, double>,
                        std::vector<std::pair<uint8_t, double>>,
                        decltype(comparator)> pTable { comparator };
    for (size_t i = 0; i < freqs.size(); ++i) {
        if (freqs[i] > 0) {
            pTable.push(std::make_pair<uint8_t, double>(
                    i, static_cast<double>(freqs[i])/bytesCount));
        }
    }

    std::unordered_map<uint8_t, ShannonCode> shannonTable {};
    double cumP = 0.0;
    while (!pTable.empty()) {
        auto&&[sym, p] = pTable.top();
        shannonTable[sym] = ShannonCode { sym, p, cumP };
        cumP += p;
        pTable.pop();
    }

    in.seekg(startPos);
    if (!in.good())
        throw std::runtime_error { "stream search failure" };

    uint32_t tableSize = shannonTable.size();
    out.write(reinterpret_cast<char*>(&tableSize), sizeof(tableSize));
    for (auto&& [_, shCode] : shannonTable) {
        out.put(static_cast<char>(shCode.getOrigin()));
        out.put(static_cast<char>(shCode.getLengthBits()));

        uint32_t codeValue = shCode.getValue();
        out.write(reinterpret_cast<char*>(&codeValue), sizeof(codeValue));
    }

    bytesCount = 0;
    // Number of unused bits in the last block
    uint32_t extraBits = 0;

    // Reserve space for them
    auto dataSizePos = out.tellp();
    out.write(reinterpret_cast<char*>(&bytesCount), sizeof(bytesCount));
    out.put(reinterpret_cast<char&>(extraBits));

    OutputBitStream resultCode { out };
    while (in.get(byte)) {
        ShannonCode shCode = shannonTable[byte];
        size_t lengthBits = shCode.getLengthBits();
        for (size_t i = 0; i < lengthBits; ++i)
            resultCode.write((shCode.getValue() >> (lengthBits - 1 - i)) & 1);
        extraBits += lengthBits;

        bytesCount += extraBits/CHAR_BIT;
        extraBits %= CHAR_BIT;
    }
    resultCode.flush();

    out.seekp(dataSizePos);
    out.write(reinterpret_cast<char*>(&bytesCount), sizeof(bytesCount));
    out.put(reinterpret_cast<char&>(extraBits));
}

void ShannonEncoder::decode(std::istream& in, std::ostream& out) {
    if (!in.good())
        throw std::invalid_argument { "input stream is in a bad state" };

    if (!out.good())
        throw std::invalid_argument { "output stream is in a bad state" };

    uint32_t tableSize;
    in.read(reinterpret_cast<char*>(&tableSize), sizeof(tableSize));

    ShannonTrie shTrie {};
    for (size_t i = 0; i < tableSize; ++i) {
        uint8_t origin, lengthBits;
        uint32_t value;

        in.get(reinterpret_cast<char&>(origin));
        in.get(reinterpret_cast<char&>(lengthBits));
        in.read(reinterpret_cast<char*>(&value), sizeof(value));

        shTrie.addCode(ShannonCode { origin, lengthBits, value });
    }

    uint32_t bytesCount;
    in.read(reinterpret_cast<char*>(&bytesCount), sizeof(bytesCount));

    uint8_t extraBits;
    in.get(reinterpret_cast<char&>(extraBits));

    InputBitStream inputData { in };
    uint32_t curBytes = 0;
    uint32_t curBits = 0;
    while (curBytes < bytesCount || curBits < extraBits) {
        ShannonCode shCode = shTrie.searchCode(inputData);
        out.put(static_cast<char>(shCode.getOrigin()));
        curBits += shCode.getLengthBits();

        curBytes += curBits/CHAR_BIT;
        curBits %= CHAR_BIT;
    }
}

} // pudge
