/**
 * pudge
 * Copyright (C) 2022  Georgij Krajnyukov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <pudge/InputBitStream.hpp>
#include <stdexcept>

namespace pudge {

InputBitStream::InputBitStream(std::istream& in, size_t bufferSizeBytes/*=kDefaultBufferSizeBytes*/)
    :inputStream(in), bits(bufferSizeBytes*CHAR_BIT) {
    if (!inputStream)
        throw std::invalid_argument { "stream is in a bad state" };

    readData();
}

InputBitStream::operator bool() const {
    return good();
}

bool InputBitStream::eof() const {
    return atBufferEnd() && inputStream.eof();
}

bool InputBitStream::fail() const {
    return atBufferEnd() && inputStream.fail();
}

bool InputBitStream::bad() const {
    return atBufferEnd() && inputStream.bad();
}

bool InputBitStream::good() const {
    return !atBufferEnd() || inputStream.good();
}

bool InputBitStream::test() {
    if (atBufferEnd())
        readData();

    return bits.test(pos++);
}

bool InputBitStream::atBufferEnd() const {
    return pos >= bits.size();
}

void InputBitStream::readData() {
    if (!inputStream)
        throw std::runtime_error { "stream is in a bad state" };

    std::vector<uint8_t> buffer(bits.size()/CHAR_BIT);
    inputStream.read(reinterpret_cast<char*>(buffer.data()), buffer.size());
    bits.clear();
    bits.init_from_block_range(buffer.cbegin(), buffer.cend());
    pos = 0;
}

} // pudge
